FROM debian:bookworm AS runtime
RUN apt-get update && apt-get upgrade -y && apt-get install -y ca-certificates openssl && ldconfig /usr/local/lib64/

FROM rust:latest AS rust_build
RUN cargo install cargo-build-deps
RUN cargo new app
WORKDIR /app
COPY Cargo.toml Cargo.lock ./
RUN cargo build-deps --release
COPY src ./src
RUN cargo build --release

FROM runtime
WORKDIR /app
COPY templates ./templates
COPY --from=rust_build /app/target/release/polysearch ./polysearch
CMD ["./polysearch"]
