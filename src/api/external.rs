use rocket::{
    http::Status,
    response::status::{Custom, NotFound},
    serde::json::Json,
    Route, State,
};

use crate::utilities::{
    data_store::{DataStore, DataStoreError},
    epfl_ldap::{self, EpflLdapUnit, EpflPerson},
};

pub fn get_routes() -> Vec<Route> {
    return routes![
        get_person_by_sciper,
        get_persons,
        get_person_by_username,
        get_unit
    ];
}

#[get("/external/epfl-person/sciper/<sciper>")]
async fn get_person_by_sciper(
    data: &State<DataStore>,
    sciper: &str,
) -> Result<Json<EpflPerson>, Status> {
    let person = data.fetch_epfl_person(sciper).await.map_err(|e| match e {
        DataStoreError::NotFound => Status::NotFound,
        DataStoreError::InternalError(_s) => Status::InternalServerError,
    })?;

    Ok(Json(person))
}

#[get("/external/epfl-persons?<scipers>")]
async fn get_persons(
    data: &State<DataStore>,
    scipers: &str,
) -> Result<Json<Vec<Option<EpflPerson>>>, Status> {
    let scipers: Vec<String> = scipers.split(",").map(String::from).collect();
    let persons = data
        .fetch_epfl_persons(&scipers)
        .await
        .map_err(|_e| Status::InternalServerError)?;
    Ok(Json(persons))
}

#[get("/external/epfl-person/username/<username>")]
async fn get_person_by_username(username: &str) -> Result<Json<EpflPerson>, NotFound<String>> {
    let result = epfl_ldap::get_person(username, epfl_ldap::PersonQueryType::Username)
        .await
        .map_err(|e| NotFound(e.to_string()))?;
    Ok(Json(result))
}

#[get("/external/epfl-unit/<acronym>")]
async fn get_unit(
    data: &State<DataStore>,
    acronym: &str,
) -> Result<Json<EpflLdapUnit>, NotFound<String>> {
    let unit = data
        .fetch_epfl_unit(acronym)
        .await
        .map_err(|e| NotFound(e.to_string()))?;

    Ok(Json(unit))
}
