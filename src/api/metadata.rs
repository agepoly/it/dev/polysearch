use rocket::{serde::json::Json, Route, State};

use crate::utilities::data_store::{DataStore, DataStoreCache, DataStoreCacheKey};

pub fn get_routes() -> Vec<Route> {
    return routes![health_check, dump_cache, dump_cache_keys];
}

#[get("/health-check")]
fn health_check() -> &'static str {
    "Ok!"
}

#[get("/metadata/cache")]
async fn dump_cache(data: &State<DataStore>) -> Json<DataStoreCache> {
    let cache = data.clone_cache();
    Json(cache)
}

#[get("/metadata/cache-keys")]
async fn dump_cache_keys(data: &State<DataStore>) -> Json<Vec<(DataStoreCacheKey, String)>> {
    let cache = data.clone_cache();
    Json(cache.get_cache_keys())
}
