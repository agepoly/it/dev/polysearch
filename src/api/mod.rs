mod external;
mod metadata;

use rocket::Route;

pub fn get_routes() -> Vec<Route> {
    let mut routes = vec![];
    routes.append(&mut self::external::get_routes());
    routes.append(&mut self::metadata::get_routes());
    return routes
}