use rocket_dyn_templates::{Template, context};

#[macro_use] extern crate rocket;

mod utilities;
mod api;

use crate::utilities::config::config;
use crate::utilities::{data_store::DataStore, runner::Runner};

#[get("/")]
fn index() -> Template {
    Template::render("base", context! { field: "value" })
}

#[launch]
fn rocket() -> _ {
    let logger_config = env_logger::Env::default().default_filter_or("polysearch=info,rocket=info");
    env_logger::Builder::from_env(logger_config).init();
    config();
    
    let (sender, receiver) = std::sync::mpsc::channel();
    let data_store = DataStore::new(sender);
    Runner::run(data_store.clone(), receiver);

    rocket::build()
        .manage(data_store)
        .attach(Template::fairing())
        .mount("/", routes![index])
        .mount("/api", api::get_routes())
}