use log::info;
use std::sync::OnceLock;
use envconfig::Envconfig;

#[derive(Envconfig)]
pub struct Config {
    #[envconfig(from = "API_KEY")]
    pub api_key: String,
    #[envconfig(from = "CACHE_EXPIRATION")]
    pub cache_expiration: Option<u64>,
}

/// Uses a [`OnceLock`] such that the config is only parsed once.
static CONFIG: OnceLock<Config> = OnceLock::new();

/// Retrieves the config through environment variables. Called on startup to check that the config is valid.
/// All accesses to environment variables must be done through this API.
pub fn config() -> &'static Config {
    CONFIG.get_or_init(|| {
        if let Ok(path) = dotenv::dotenv() {
            info!("Loaded .env file (at {:?})", path);
        }

        Envconfig::init_from_env().unwrap()
    })
}
