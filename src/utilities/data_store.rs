use std::{
    collections::HashMap,
    hash::Hash,
    sync::{mpsc::Sender, Arc, Mutex},
    time::{Duration, Instant},
};

use log::debug;
use rocket::{futures::future::join_all, response::status::NotFound};
use serde::Serialize;

use super::runner::RunnerTask;
use super::{
    config::config,
    epfl_ldap::{self, EpflLdapError, EpflLdapUnit, EpflPerson, PersonQueryType},
};

#[derive(Clone)]
pub struct DataStore {
    cache: Arc<Mutex<DataStoreCache>>,
    runner_sender: Sender<RunnerTask>,
}

impl DataStore {
    pub fn new(runner_sender: Sender<RunnerTask>) -> Self {
        Self {
            cache: Arc::new(Mutex::new(DataStoreCache::new())),
            runner_sender,
        }
    }

    pub async fn fetch_epfl_persons(
        &self,
        scipers: &Vec<String>,
    ) -> Result<Vec<Option<EpflPerson>>, DataStoreError> {
        let mut persons: Vec<Option<EpflPerson>> = vec![];

        for sciper in scipers {
            match self.fetch_epfl_person(sciper).await {
                Ok(person) => persons.push(Some(person)),
                Err(DataStoreError::NotFound) => {
                    persons.push(None)
                },
                Err(DataStoreError::InternalError(s)) => return Err(DataStoreError::InternalError(s)),
            }
        }

        Ok(persons)

        // join_all(
        //     scipers
        //         .into_iter()
        //         .map(|sciper| self.fetch_epfl_person(sciper)),
        // )
        // .await
        // .into_iter()
        // .map(|person_result| match person_result {
        //     Ok(person) => Ok(Some(person)),
        //     Err(DataStoreError::NotFound) => Ok(None),
        //     Err(DataStoreError::InternalError(s)) => Err(DataStoreError::InternalError(s)),
        // })
        // .collect()
    }

    pub async fn fetch_epfl_person(&self, sciper: &str) -> Result<EpflPerson, DataStoreError> {
        let cached_person = self.get_cached_epfl_person(sciper);
        if let Some(person) = cached_person {
            if self.check_cached_username(&person.username) {
                return Ok(person);
            }
        }

        let person = match epfl_ldap::get_person(sciper, PersonQueryType::Sciper).await {
            Ok(person) => Ok(person),
            Err(EpflLdapError::NotFound) => Err(DataStoreError::NotFound),
            Err(_) => Err(DataStoreError::InternalError(
                "LDAP internal error".to_string(),
            )),
        }?;

        self.set_cached_epfl_person(person.clone());

        person.accreds.iter().for_each(|a| {
            if !self.check_cached_unit_members(&a.0) {
                self.queue_unit_members(&a.0);
            }
        });

        Ok(person)
    }

    pub async fn fetch_epfl_unit(&self, acronym: &str) -> Result<EpflLdapUnit, DataStoreError> {
        let cached_unit = self.get_cached_epfl_unit(acronym);
        if let Some(unit) = cached_unit {
            if self.check_cached_unit_members(acronym) {
                return Ok(unit);
            }
        }

        let unit = epfl_ldap::get_unit(acronym).await.map_err(|e| match e {
            EpflLdapError::NotFound => DataStoreError::NotFound,
            _ => DataStoreError::InternalError("LDAP internal error".to_string()),
        })?;
        self.set_cached_epfl_unit(unit.clone());

        Ok(unit)
    }

    pub fn get_cached_epfl_person(&self, sciper: &str) -> Option<EpflPerson> {
        let instance = &self.cache.lock().unwrap();
        match instance.epfl_persons.get(sciper).cloned() {
            Some(person) => {
                debug!("cache hit person sciper={}", sciper);
                Some(person)
            }
            None => {
                debug!("cache miss person sciper={}", sciper);
                None
            }
        }
    }

    pub fn set_cached_epfl_person(&self, person: EpflPerson) {
        let instance = &mut self.cache.lock().unwrap();
        instance.cache_keys.insert(
            (DataStoreCacheKey::Username, person.username.to_owned()),
            Instant::now(),
        );
        instance
            .epfl_persons
            .insert(person.sciper.to_owned(), person);
    }

    pub fn set_cached_epfl_persons(&self, persons: Vec<EpflPerson>) {
        let instance = &mut self.cache.lock().unwrap();
        for person in persons {
            instance.cache_keys.insert(
                (DataStoreCacheKey::Username, person.username.to_owned()),
                Instant::now(),
            );
            instance
                .epfl_persons
                .insert(person.sciper.to_owned(), person);
        }
    }

    pub fn get_cached_epfl_unit(&self, acronym: &str) -> Option<EpflLdapUnit> {
        let instance = &self.cache.lock().unwrap();
        match instance.epfl_units.get(acronym).cloned() {
            Some(unit) => {
                debug!("cache hit unit acronym={}", acronym);
                Some(unit)
            }
            None => {
                debug!("cache miss unit acronym={}", acronym);
                None
            }
        }
    }

    pub fn set_cached_epfl_unit(&self, unit: EpflLdapUnit) {
        let instance = &mut self.cache.lock().unwrap();
        instance.cache_keys.insert(
            (DataStoreCacheKey::Unit, unit.acronym.to_owned()),
            Instant::now(),
        );
        if !unit.members.is_empty() && instance.check_cached_unit_members(&unit.acronym) {
            self.queue_unit_members(&unit.acronym);
        }
        instance.epfl_units.insert(unit.acronym.to_owned(), unit);
    }

    pub fn queue_unit_members(&self, unit_acronym: &str) {
        debug!("Queuing unit members of unit {}", unit_acronym);
        let task = RunnerTask::EpflUnitMembersLookup(unit_acronym.to_string());
        self.runner_sender
            .send(task)
            .expect("Unable to queue RunnerTask");
        self.set_cached_unit_members(unit_acronym);
    }

    pub fn check_cached_username(&self, username: &str) -> bool {
        self.cache.lock().unwrap().check_cached_username(username)
    }

    pub fn check_cached_unit_members(&self, acronym: &str) -> bool {
        self.cache
            .lock()
            .unwrap()
            .check_cached_unit_members(acronym)
    }

    pub fn set_cached_unit_members(&self, acronym: &str) {
        self.cache.lock().unwrap().cache_keys.insert(
            (DataStoreCacheKey::UnitMembers, acronym.to_string()),
            Instant::now(),
        );
    }

    pub fn clone_cache(&self) -> DataStoreCache {
        self.cache.lock().unwrap().clone()
    }
}

#[derive(Serialize, Clone, Debug, Hash, PartialEq, Eq)]
pub enum DataStoreCacheKey {
    Username,
    Sciper,
    UnitMembers,
    Unit,
}

#[derive(Serialize, Clone, Debug)]
pub struct DataStoreCache {
    epfl_persons: HashMap<String, EpflPerson>,
    epfl_units: HashMap<String, EpflLdapUnit>,

    /// maps usernames to
    #[serde(skip_serializing)]
    cache_keys: HashMap<(DataStoreCacheKey, String), Instant>,
}

impl DataStoreCache {
    fn new() -> Self {
        Self {
            epfl_persons: HashMap::new(),
            cache_keys: HashMap::new(),
            epfl_units: HashMap::new(),
        }
    }

    pub fn get_cache_keys(&self) -> Vec<(DataStoreCacheKey, String)> {
        self.cache_keys.iter().map(|(k, _v)| k.to_owned()).collect()
    }

    pub fn check_cached_username(&self, username: &str) -> bool {
        match self
            .cache_keys
            .get(&(DataStoreCacheKey::Username, username.to_string()))
        {
            Some(instant) => {
                let duration = Instant::now().duration_since(*instant).as_secs();
                let expiration = config().cache_expiration.unwrap_or(3600 * 12);
                let expired = duration >= expiration;
                if expired {
                    debug!(
                        "check_cached_username: expired ({}s >= {}s)",
                        duration, expiration
                    );
                }
                !expired
            }
            None => {
                // debug!("check_cached_username: key not found");
                false
            }
        }
    }

    pub fn check_cached_unit_members(&self, acronym: &str) -> bool {
        match self
            .cache_keys
            .get(&(DataStoreCacheKey::UnitMembers, acronym.to_string()))
        {
            Some(instant) => {
                let duration = Instant::now().duration_since(*instant).as_secs();
                let expiration = config().cache_expiration.unwrap_or(3600 * 12);
                let expired = duration >= expiration;
                if expired {
                    debug!(
                        "check_cached_unit_members: expired ({}s >= {}s)",
                        duration, expiration
                    );
                }
                !expired
            }
            None => {
                // debug!("check_cached_unit_members: key not found");
                false
            }
        }
    }
}

#[derive(Debug)]
pub enum DataStoreError {
    NotFound,
    InternalError(String),
}

impl ToString for DataStoreError {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}
