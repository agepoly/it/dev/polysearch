use std::collections::HashMap;

use ldap3::{LdapConnAsync, LdapError, ResultEntry, SearchEntry};
use log::{debug, warn};
use serde::Serialize;

#[derive(Debug)]
pub enum EpflLdapError {
    ConnectionError(String),
    ParsingError,
    NotFound,
    TooManyResults,
}

impl From<LdapError> for EpflLdapError {
    fn from(e: LdapError) -> Self {
        EpflLdapError::ConnectionError(e.to_string())
    }
}

impl ToString for EpflLdapError {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}

fn maybe_get_first_attribute(entry: &SearchEntry, attr: &str) -> Option<String> {
    entry.attrs.get(attr).map(|v| v.first().cloned()).flatten()
}

fn get_first_attribute(entry: &SearchEntry, attr: &str) -> Result<String, EpflLdapError> {
    match maybe_get_first_attribute(entry, attr) {
        Some(a) => Ok(a),
        None => Err(EpflLdapError::ParsingError),
    }
}

#[derive(Serialize, Clone)]
pub struct EpflLdapPerson {
    pub sciper: String,
    pub email: Option<String>,
    pub username: String,
    pub firstname: String,
    pub lastname: String,
    pub groups: Vec<String>,
    pub unit: String,
    pub function_en: Option<String>,
    pub function_fr: Option<String>,
}

impl TryFrom<ResultEntry> for EpflLdapPerson {
    fn try_from(e: ResultEntry) -> Result<Self, Self::Error> {
        let e = SearchEntry::construct(e);
        Ok(Self {
            sciper: get_first_attribute(&e, "uniqueIdentifier")?,
            email: maybe_get_first_attribute(&e, "mail"),
            username: get_first_attribute(&e, "uid")?,
            firstname: get_first_attribute(&e, "givenName")?,
            lastname: get_first_attribute(&e, "sn")?,
            groups: e
                .attrs
                .get("memberOf")
                .ok_or(EpflLdapError::ParsingError)?
                .to_owned(),
            unit: get_first_attribute(&e, "ou")?,
            function_fr: maybe_get_first_attribute(&e, "title"),
            function_en: maybe_get_first_attribute(&e, "title;lang-en"),
        })
    }

    type Error = EpflLdapError;
}

#[derive(Serialize, Debug, Clone)]
pub struct EpflPerson {
    pub sciper: String,
    pub email: Option<String>,
    pub username: String,
    pub firstname: String,
    pub lastname: String,
    pub groups: Vec<String>,
    pub accreds: Vec<(String, Option<String>, Option<String>)>,
}

#[derive(Serialize, Debug)]
pub enum PersonQueryType {
    Sciper,
    Username,
}

pub async fn get_person(
    query: &str,
    query_type: PersonQueryType,
) -> Result<EpflPerson, EpflLdapError> {
    info!("get_person request: query={}, type:{:?}", query, query_type);
    let (conn, mut ldap) = LdapConnAsync::new("ldaps://ldap.epfl.ch").await?;
    ldap3::drive!(conn);
    let filter = match query_type {
        PersonQueryType::Sciper => format!(
            "(&(uniqueIdentifier={})(objectClass=EPFLorganizationalPerson))",
            query
        ),
        PersonQueryType::Username => {
            format!("(&(uid={})(objectClass=EPFLorganizationalPerson))", query)
        }
    };
    let (entries, _res) = ldap
        .search("c=ch", ldap3::Scope::Subtree, &filter, vec!["*"])
        .await?
        .success()?;
    let number_of_entries = entries.len();
    let persons: Vec<EpflLdapPerson> = entries
        .into_iter()
        .filter_map(|e| EpflLdapPerson::try_from(e.clone()).ok())
        .collect();
    if persons.len() != number_of_entries {
        warn!(
            "get_person request: query={} resulted in {} entries but {} persons",
            query,
            number_of_entries,
            persons.len()
        );
    }
    ldap.unbind().await?;

    if persons.is_empty() {
        return Err(EpflLdapError::NotFound);
    }

    let mut person = EpflPerson {
        sciper: persons.first().unwrap().sciper.to_owned(),
        email: persons.first().unwrap().email.to_owned(),
        username: persons.first().unwrap().username.to_owned(),
        firstname: persons.first().unwrap().firstname.to_owned(),
        lastname: persons.first().unwrap().lastname.to_owned(),
        groups: persons.first().unwrap().groups.to_owned(),
        accreds: vec![],
    };

    for p in persons {
        if p.sciper != person.sciper {
            return Err(EpflLdapError::TooManyResults);
        }
        person.accreds.push((p.unit, p.function_fr, p.function_en))
    }

    Ok(person)
}

#[derive(Serialize, Debug, Clone)]
pub struct EpflLdapUnit {
    pub id: String,
    pub acronym: String,
    pub name: String,
    /// members represented by gaspar usernames
    pub members: Vec<String>,
}

impl TryFrom<ResultEntry> for EpflLdapUnit {
    fn try_from(e: ResultEntry) -> Result<Self, Self::Error> {
        let e = SearchEntry::construct(e);
        Ok(Self {
            id: get_first_attribute(&e, "uniqueIdentifier")?,
            acronym: get_first_attribute(&e, "cn")?,
            name: get_first_attribute(&e, "description")?,
            members: e.attrs.get("memberUid").unwrap_or(&vec![]).to_owned(),
        })
    }

    type Error = EpflLdapError;
}

pub async fn get_unit(unit: &str) -> Result<EpflLdapUnit, EpflLdapError> {
    info!("get_unit request: unit={}", unit);
    let (conn, mut ldap) = LdapConnAsync::new("ldaps://ldap.epfl.ch").await?;
    ldap3::drive!(conn);
    let filter = format!("(&(ou={})(objectClass=EPFLorganizationalUnit))", unit);
    let (entries, _res) = ldap
        .search("c=ch", ldap3::Scope::Subtree, &filter, vec!["*"])
        .await?
        .success()?;

    let units: Vec<EpflLdapUnit> = entries
        .into_iter()
        .filter_map(|e| EpflLdapUnit::try_from(e).ok())
        .collect();
    ldap.unbind().await?;

    if units.is_empty() {
        return Err(EpflLdapError::NotFound);
    } else if units.len() > 1 {
        return Err(EpflLdapError::TooManyResults);
    }
    Ok(units[0].clone())
}

pub async fn get_persons_from_unit(acronym: &str) -> Result<Vec<EpflPerson>, EpflLdapError> {
    info!("get_persons_from_unit request: unit={}", acronym);
    let (conn, mut ldap) = LdapConnAsync::new("ldaps://ldap.epfl.ch").await?;
    ldap3::drive!(conn);
    let filter = format!("(&(ou={})(objectClass=EPFLorganizationalPerson))", acronym);
    let (entries, _res) = ldap
        .search("c=ch", ldap3::Scope::Subtree, &filter, vec!["*"])
        .await?
        .success()?;

    let number_of_entries = entries.len();
    let ldap_persons: Vec<EpflLdapPerson> = entries
        .into_iter()
        .filter_map(|e| EpflLdapPerson::try_from(e.clone()).ok())
        .collect();
    if ldap_persons.len() != number_of_entries {
        warn!(
            "get_persons_from_unit request: unit={} resulted in {} entries but {} persons",
            acronym,
            number_of_entries,
            ldap_persons.len()
        );
    }

    let mut persons_map: HashMap<String, EpflPerson> = HashMap::new();
    ldap_persons.into_iter().for_each(|p| {
        if let Some(prev_p) = persons_map.get_mut(&p.sciper) {
            prev_p.accreds.push((p.unit, p.function_fr, p.function_en));
        } else {
            persons_map.insert(p.sciper.clone(), EpflPerson {
                sciper: p.sciper.to_owned(),
                email: p.email.to_owned(),
                username: p.username.to_owned(),
                firstname: p.firstname.to_owned(),
                lastname: p.lastname.to_owned(),
                groups: p.groups.to_owned(),
                accreds: vec![
                    (p.unit, p.function_fr, p.function_en)
                ],
            });
        }
    });

    Ok(persons_map.into_iter().map(|(_k,v)| v).collect())
}
