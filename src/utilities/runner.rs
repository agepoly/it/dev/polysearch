use log::{debug, warn};
use std::{
    sync::mpsc::{channel, Receiver},
    thread::{self, JoinHandle},
};

use super::epfl_ldap;

#[derive(Debug)]
pub enum RunnerTask {
    EpflUsernameLookup(String),
    EpflSciperLookup(String),
    EpflUnitLookup(String),
    EpflUnitMembersLookup(String),
}

pub struct Runner {}

impl Runner {
    pub fn run(data_store: crate::DataStore, receiver: Receiver<RunnerTask>) -> Self {
        let runtime =
            rocket::tokio::runtime::Runtime::new().expect("Runner unable to start async runtime");
        thread::spawn(move || {
            while let Some(task) = receiver.iter().next() {
                debug!("Processing new RunnerTask {:?}", task);
                match task {
                    RunnerTask::EpflUsernameLookup(username) => {
                        let person = runtime.block_on(epfl_ldap::get_person(
                            &username,
                            epfl_ldap::PersonQueryType::Username,
                        ));
                        match person {
                            Ok(person) => {
                                data_store.set_cached_epfl_person(person);
                                debug!("Runner cached person username={}", username);
                            }
                            Err(e) => warn!("Runner error: username fetch failed with {:?}", e),
                        }
                    }
                    RunnerTask::EpflSciperLookup(sciper) => {
                        let person = runtime.block_on(epfl_ldap::get_person(
                            &sciper,
                            epfl_ldap::PersonQueryType::Sciper,
                        ));
                        match person {
                            Ok(person) => {
                                data_store.set_cached_epfl_person(person);
                                debug!("Runner cached person sciper={}", sciper);
                            }
                            Err(e) => warn!("Runner error: sciper fetch failed with {:?}", e),
                        }
                    }
                    RunnerTask::EpflUnitLookup(acronym) => {
                        let unit = runtime.block_on(epfl_ldap::get_unit(&acronym));
                        match unit {
                            Ok(unit) => {
                                data_store.set_cached_epfl_unit(unit);
                                debug!("Runner cached unit acronym={}", acronym);
                            }
                            Err(e) => warn!("Runner error: unit fetch failed with {:?}", e),
                        }
                    },
                    RunnerTask::EpflUnitMembersLookup(acronym) => {
                        if data_store.check_cached_unit_members(&acronym) {
                            debug!("Runner cache hit for {}", acronym);
                            // continue;
                        }
                        let members: Result<Vec<epfl_ldap::EpflPerson>, epfl_ldap::EpflLdapError> = runtime.block_on(epfl_ldap::get_persons_from_unit(&acronym));
                        match members {
                            Ok(members) => {
                                let count = members.len();
                                data_store.set_cached_epfl_persons(members);
                                debug!("Runner cached unit members acronym={} count={}", acronym, count);
                            },
                            Err(e) => warn!("Runner error: unit members fetch failed with {:?}", e),
                        }
                        data_store.set_cached_unit_members(&acronym);
                    }
                }
            }
        });

        Self {}
    }
}
